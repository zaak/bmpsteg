#include "BMPLib.h"

Bitmap::Bitmap(void) : data(NULL)
{
	memset(&header, 0, sizeof(header));
}

Bitmap::~Bitmap(void)
{
	if(data)
		free(data);//delete[] data;
}

bool Bitmap::Load(char* filename)
{
	FILE* f = NULL;
	f = fopen(filename, "rb");
	if(!f)
		return false;

	fseek(f, 0, SEEK_END);
	long fSize = ftell(f);
	rewind(f);

	fread(&header, 1, sizeof(header), f);

	dataSize = fSize - sizeof(header);
	data = (BYTE *)malloc(sizeof(BYTE) * dataSize);//data = new BYTE[dataSize];
	//fseek(f, sizeof(header), SEEK_SET);
	fread(data, 1, dataSize, f); 

	fclose(f);

	scanLineSize = header.biWidth * 3;
	if(scanLineSize % 4 != 0)
	{
		scanLineSize = ((scanLineSize >> 2) + 1) << 2;
	}

	return true;
}

bool Bitmap::Dump(char *filename)
{
	FILE* f = NULL;
	f = fopen(filename, "wb");
	if(!f)
		return false;
	fwrite(&header, 1, sizeof(header), f);
	fwrite(data, 1, dataSize, f);
	fclose(f);

	return true;
}

bool Bitmap::Create(int width, int height)
{
	header.bfType     = 0x4d42; // BM
	header.biSize     = 40;
	header.biHeight   = height;
	header.biWidth    = width;
	header.biPlanes   = 1;
	header.biBitCount = 24;

	scanLineSize = width * 3;
	if(scanLineSize % 4 != 0)
	{
		scanLineSize = ((scanLineSize >> 2) + 1) << 2;
	}
	dataSize = scanLineSize * height;

	header.bfOffBits = sizeof(BitmapHeader);
	header.bfSize    = sizeof(BitmapHeader) + dataSize;
	
	data = (BYTE *)malloc(sizeof(BYTE) * dataSize);
	memset(data, 0, dataSize);

	return false;
}

bool Bitmap::RGBForeach(void (* RGBForeachCallback)(RGB* rgb))
{
	if(!RGBForeachCallback)
		return false;

	for (unsigned int i = 0; i < dataSize; i += scanLineSize)
	{
		RGB* d = (RGB *)(data + i);
		for (unsigned int j = 0; j <header.biWidth; j++)
		{
			RGBForeachCallback(d + j);
		}
		//printf("row: %i\n", i);
	}
	return true;
}

void Bitmap::PrintInfo()
{
	printf("File size: %i\n", header.bfSize);
	printf("Header size: %i\n",header.biSize);
	printf("Image data size: %i\n", header.biSizeImage);
	printf("bmp.dataSize: %i\n", dataSize);
	printf("Bits per pixel: %i\n", header.biBitCount);
	printf("bmp.header.biWidth: %i\n", header.biWidth);
	printf("scanLineSize: %i\n", scanLineSize);
}
