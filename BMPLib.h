#pragma once
#pragma warning(disable : 4996) // fopen warning

#include <cstdio>
#include <cstring>
#include <cstdlib>

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned int

#pragma pack(push,1)

struct RGB
{
	BYTE b, g, r;
};

struct BitmapHeader
{
	WORD	bfType;				// Magic (BM)
	DWORD	bfSize;				// Rozmiar pliku
	DWORD	bfReserved;
	DWORD	bfOffBits;			// Offset danych obrazowych
	DWORD	biSize;				// Wielko�� nag��wka
	DWORD	biWidth;			// Szeroko�� obrazu
	DWORD	biHeight;			// Wysoko�� obrazu
	WORD	biPlanes;			// Liczba warstw kolor�w
	WORD	biBitCount;			// Liczba bit�w na piksel
	DWORD	biCompression;		// Algorytm kompresji
	DWORD	biSizeImage;		// Rozmiar samego rysunku
	DWORD	biXPelsPerMeter;	// Rozdzielczo�� pozioma
	DWORD	biYPelsPerMeter;	// Rozdzielczo�� pionowa
	DWORD	biClrUsed;			// Liczba kolor�w w palecie
	BYTE	biClrImportant;
	BYTE	biClrRotation;
	WORD	biReserved;
};

#pragma pack(pop)

class Bitmap
{
public:
	BitmapHeader header;
	BYTE*		 data;
	unsigned int dataSize;
	unsigned int scanLineSize;
	
	Bitmap(void);
	~Bitmap(void);
	bool Load(char* filename);
	bool Dump(char* filename);
	void PrintInfo();
	bool Create(int width, int height);
	bool RGBForeach(void (* RGBForeachCallback)(RGB* rgb));
};
