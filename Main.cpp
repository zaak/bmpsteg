#include <cstdio>
#include "BMPLib.h"

void usage()
{
	puts("bmpsteg - bitmap steganography tool\n"
		 "2009 (c) Pawel Wiaderny\n"
		 "\nUsage: bmpsteg <options>\n"
		 "   -c <input BMP> <mask BMP>\n"
		 "   -d <BMP file>");
}

int main(int argc, char **argv)
{
	//printf("sizeof(BitmapHeader): %i\n", sizeof(BitmapHeader));
	//printf("sizeof(RGB): %i\n\n", sizeof(RGB));

	if(!argv[1])
	{
		usage();
		return 1;
	}

	if(!strcmp(argv[1], "-c"))
	{
		if(argv[2] && argv[3])
		{
			Bitmap bmpInput, bmpMask;

			if(!bmpInput.Load(argv[2]) ||
			   !bmpMask.Load(argv[3]))
			{
				puts("Couldn't load file. Aborting...");
				return 2;
			}

			puts("Input BMP info:");
			bmpInput.PrintInfo();

			puts("\nMask BMP info:");
			bmpMask.PrintInfo();

			if(bmpInput.dataSize != bmpMask.dataSize ||
				bmpInput.scanLineSize != bmpMask.scanLineSize ||
				bmpInput.header.biWidth != bmpMask.header.biWidth)
			{
				puts("Different data size, scanLineSize or width. Aborting...");
				return 1;
			}
			
			unsigned int dataSize = bmpInput.dataSize;
			int scanLineSize = bmpInput.scanLineSize;
			unsigned int width = bmpInput.header.biWidth;

			for (unsigned int i = 0; i < dataSize; i += scanLineSize)
			{
				RGB* bi = (RGB *)(bmpInput.data + i);
				RGB* bm = (RGB *)(bmpMask.data + i);
				for (unsigned int j = 0; j < width; j++)
				{
					if(bm[j].g == 0)
						bi[j].r |= 1;
					else
						bi[j].r &= ~1;
				}
			}
			bmpInput.Dump("crypted.bmp");
			puts("\nDone. Output bitmap written to file \"crypted.bmp\".");
		}
		else
			usage();
	}
	else if(!strcmp(argv[1], "-d"))
	{
		if(argv[2])
		{
			Bitmap bmp;
			if(!bmp.Load(argv[2]))
			{
				puts("Couldn't load file. Aborting...");
				return 2;
			}

			puts("Crypted BMP info:");
			bmp.PrintInfo();

			for (unsigned int i = 0; i < bmp.dataSize; i += bmp.scanLineSize)
			{
				RGB* d = (RGB *)(bmp.data + i);
				for (unsigned int j = 0; j < bmp.header.biWidth; j++)
				{
					if(d[j].r & 1)
					{
						d[j].r = ~d[j].r;
						d[j].g = ~d[j].g;
						d[j].b = ~d[j].b;
					}
				}
			}

			bmp.Dump("decrypted.bmp");
			puts("\nDone. Output bitmap written to file \"decrypted.bmp\".");
		}
		else
			usage();
	}
	else
		usage();

	//getchar();
	return 0;
}
